
package net.mcreator.helplcraft.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcreator.helplcraft.item.ItemHelplium;
import net.mcreator.helplcraft.block.BlockHelpliumOre;
import net.mcreator.helplcraft.ElementsHelplcraft;

@ElementsHelplcraft.ModElement.Tag
public class RecipeHelplsmelt extends ElementsHelplcraft.ModElement {
	public RecipeHelplsmelt(ElementsHelplcraft instance) {
		super(instance, 3);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockHelpliumOre.block, (int) (1)), new ItemStack(ItemHelplium.block, (int) (1)), 2F);
	}
}
