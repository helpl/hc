package net.mcreator.helplcraft.procedure;

import net.minecraft.potion.PotionEffect;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.Entity;

import net.mcreator.helplcraft.potion.PotionFakeSlowFalling;
import net.mcreator.helplcraft.ElementsHelplcraft;

@ElementsHelplcraft.ModElement.Tag
public class ProcedureFakeSlowFallingOnPotionActiveTick extends ElementsHelplcraft.ModElement {
	public ProcedureFakeSlowFallingOnPotionActiveTick(ElementsHelplcraft instance) {
		super(instance, 43);
	}

	public static void executeProcedure(java.util.HashMap<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure FakeSlowFallingOnPotionActiveTick!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof EntityLivingBase)
			((EntityLivingBase) entity).addPotionEffect(new PotionEffect(PotionFakeSlowFalling.potion, (int) 0.5, (int) 0));
	}
}
